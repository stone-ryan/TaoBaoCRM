package com.meilian.tbcrm.java.tmc.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Jedis数据源
 *
 * @author 陈杰
 * @since 2016年9月12日 00:53:51
 * @version v0.1
 *
 * Copyright ChenJie(chenjie_java@aliyun.com) 2016年9月12日 00:53:51
 */
public class JedisDataSource {
    private static final Logger logger = LoggerFactory.getLogger(JedisDataSource.class);
    
    private JedisPool jedisPool;

    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    /**
     * 获取Jedis客户端
     *
     * @return Jedis
     * */
    public synchronized Jedis getResource() {
        Jedis jedis = jedisPool.getResource();
        if(jedis != null) {
            return jedis;
        }
        return null;
    }

    /**
     * 释放Redis客户端
     *
     * @param jedis
     * */
    public synchronized void returnResource(Jedis jedis){
        if(jedis!=null) {
            jedis.close();
        }
    }
}