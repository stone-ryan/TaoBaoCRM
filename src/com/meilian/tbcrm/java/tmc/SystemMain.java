package com.meilian.tbcrm.java.tmc;

import com.meilian.tbcrm.java.tmc.message.TmcClientManager;
import com.meilian.tbcrm.java.tmc.message.TmcClientMessageHanlder;

import com.meilian.tbcrm.java.tmc.util.Config;
import com.taobao.api.internal.tmc.TmcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 淘宝java插件,项目启动类
 *
 * @author 陈杰
 *
 */
public class SystemMain {

    private static Logger logger = LoggerFactory.getLogger(SystemMain.class);


    public static void main(String[] args){
        try {
            (new TmcClientManager()).startThread();
        }catch (Exception ex){
            logger.info("系统启动出现异常,异常信息:{}",ex.getMessage(),ex);
        }
    }


}
